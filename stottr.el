;; major mode for stottr 
;; SYNTAX
(setq stottr-font-lock-keywords
      (let* (
            ;; type key words
            (x-types '("rdfs:Resource" "ottr:IRI" "owl:Class" "owl:NamedIndividual" "rdfs:Datatype" "owl:ObjectProperty" "owl:DatatypeProperty" "owl:AnnotationProperty" "rdfs:Literal" "xsd:string" "xsd:normalizedString" "xsd:token" "xsd:language" "xsd:Name" "xsd:NCName" "xsd:NMTOKEN" "owl:real" "owl:rational" "xsd:decimal" "xsd:integer" "xsd:long" "xsd:int" "xsd:short" "xsd:byte" "xsd:nonNegativeInteger" "xsd:positiveInteger" "xsd:unsignedLong" "xsd:unsignedInt" "xsd:unsignedShort" "xsd:unsignedByte" "xsd:nonPositiveInteger" "xsd:negativeInteger" "xsd:double" "xsd:float" "xsd:date" "xsd:dateTime" "xsd:dateTimeStamp" "xsd:time" "xsd:gYear" "xsd:gMonth" "xsd:gDay" "xsd:gYearMonth" "xsd:gMonthDay" "xsd:duration" "xsd:yearMonthDuration" "xsd:dayTimeDuration" "xsd:hexBinary" "xsd:base64Binary" "xsd:boolean" "xsd:anyURI" "rdf:HTML" "rdf:XMLLiteral" "NEList" "List" ))
	    ;;expansion mode keywords
            (x-exp-modes '("cross" "zipMax" "zipMin" "\+\+"))
            ;; generate regex string for each category of keywords
            (x-prefix-regexp "[[:alnum:]-_]*:")
            (x-types-regexp (regexp-opt x-types 'words))
            (x-keywords-regexp "@prefix")
            (x-exp-modes-regexp (regexp-opt x-exp-modes 'words))
	    (x-variables-regexp "?[[:graph:]]*"))
        `(
	  ;; comments
          ("^\\s-*\\(#.*\\)" 0 font-lock-comment-face t)
	  ;;blank nodes
	  ("[^[:word:]]\\(_:[A-Za-z0-9]*\\)" 1 font-lock-constant-face t) 
	  ;;types
          (,x-types-regexp . font-lock-type-face)
	  ;;prefixes
          (,x-prefix-regexp . font-lock-builtin-face)
	  ;;prefix IRIs
          ("<.*?>[[:blank:]]*\\." 0 font-lock-function-name-face t)
	  ;;keywords
          (,x-keywords-regexp . font-lock-constant-face)
	  ;;expansion modes
          (,x-exp-modes-regexp . font-lock-function-name-face)
	  ;;variables
	  (,x-variables-regexp . font-lock-variable-name-face)
          ;; note: order above matters, because once colored, that part won't change.
          ;; in general, put longer words first
          )))

(defvar stottr-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-c C-l") 'stottr-lutra-library)
    (define-key map (kbd "C-c C-i") 'stottr-lutra-instance)
         map) "Keymap for `stottr-mode'")
;; (when (not stottr-mode-map)
;;   (setq stottr-mode-map (make-sparse-keymap))
;;   (define-key stottr-mode-map (kbd "C-c C-c") 'stottr-cmd1)
;;   (define-key stottr-mode-map (kbd "C-c C-;") 'my-comment-dwim)
;;   (define-key stottr-mode-map [remap comment-dwim] 'my-comment-dwim)
  ;; by convention, major mode's keys should begin with the form C-c C-‹key›
  ;; by convention, keys of the form C-c ‹letter› are reserved for user. don't define such keys in your major mode
  

(define-derived-mode stottr-mode prog-mode "stottr mode"
  "Major mode for editing stOTTR files…"
;  (interactive)
  (kill-all-local-variables)
  ;; code for syntax highlighting
  (setq font-lock-defaults '((stottr-font-lock-keywords)))
  (use-local-map stottr-mode-map)
  (setq comment-start "#")
  (setq comment-end "")
  )

;;COMMANDS

;;library expansion and format. Can store options in local variables library-mode, library-output-format, and library-output-location
(defun stottr-lutra-library (mode outputFormat outputLocation store)
  "Expand/Format stOTTR library in buffer's folder"
  (interactive  (let ((completion-ignore-case  t))
		  (list (if (boundp 'library-mode) library-mode (completing-read "Enter expansion command (expandLibrary, formatLibrary): " '("expandLibrary" "formatLibrary") nil t))
;			(completing-read "Enter library format (stottr, wottr, legacy): " '("stottr" "wottr" "legacy") nil t)
		  (if (boundp 'library-output-format) library-output-format (completing-read "Enter output format (stottr, wottr, legacy): " '("stottr" "wottr" "legacy") nil t))
		  (if (boundp 'library-output-location) library-output-location (read-string "Enter output location: "))
		  (when (not (and (boundp 'library-output-format) (boundp 'library-mode) (boundp 'library-output-location))) (completing-read "Store as local variables? (yes or no) " '("yes" "no") nil t))
)))
;store options as file local variables					;
  (when (string= store "yes") (funcall-interactively 'add-file-local-variable 'library-mode mode) (funcall-interactively 'add-file-local-variable 'output-format outputFormat) (funcall-interactively 'add-file-local-variable 'library-output-location outputLocation) )
  ;run shell command
  (shell-command (concat "java -jar " stottr-lutra-path " -L stottr"
			 " -m "
			 mode
			 " -l . "
			 " -f -F wottr -O "
			 outputFormat
			 " -o "
			 outputLocation)))

(defun stottr-lutra-instance (mode libFormat libLoc outputFormat outputLoc store)
  "Expand stOTTR file in buffer"
  (interactive  (let ((completion-ignore-case  t))
		  
;check whether need input, get input if needed
		  (list (if (boundp 'instance-mode) instance-mode (completing-read "Choose command (expand, format): " '("expand" "format") nil t))
			(if (boundp 'instance-library-format) instance-library-format (completing-read "Enter library format (stottr, wottr, legacy): " '("stottr" "wottr" "legacy") nil t))
			(if (boundp 'instance-library-location) instance-library-location (read-string "Enter library location: "))
			(if (boundp 'instance-output-format) instance-output-format (completing-read "Enter output format (stottr, wottr, legacy): " '("stottr" "wottr" "legacy") nil t))
			(if (boundp 'instance-output-location) instance-output-location (read-string "Enter output location: "))

;check if local variables should be saved
		  (when (not (and (boundp 'instance-output-format) (boundp 'instance-mode) (boundp 'instance-library-location) (boundp 'instance-output-location) (boundp 'instance-library-format))) (completing-read "Store as local variables? (yes or no) " '("yes" "no") nil t))
		  )))
 ;store options as local variables 
  (when (string= store "yes") (funcall-interactively 'add-file-local-variable 'instance-mode mode) (funcall-interactively 'add-file-local-variable 'instance-output-format outputFormat) (funcall-interactively 'add-file-local-variable 'instance-library-format libFormat) (funcall-interactively 'add-file-local-variable 'instance-library-location libLoc) (funcall-interactively 'add-file-local-variable 'instance-output-location outputLoc))
  ;run shell command
  (shell-command (concat "java -jar " stottr-lutra-path " -I stottr -L "
			 libFormat
			 " -m "
			 mode
			 " -l "
			 libLoc
			 " -f -F wottr -O "
			 outputFormat
			 " -o "
			 outputLoc " " (buffer-file-name))))

;;comments
;(setq-local comment-start "# ")
;(setq-local comment-end "")
(defun my-comment-dwim ()
  "Comment or uncomment the current line or text selection."
  (interactive)
  ;; If there's no text selection, comment or uncomment the line
  ;; depending whether the WHOLE line is a comment. If there is a text
  ;; selection, using the first line to determine whether to
  ;; comment/uncomment.
  (let (p1 p2)
    (if (use-region-p)
        (save-excursion
          (setq p1 (region-beginning) p2 (region-end))
          (goto-char p1)
          (if (wholeLineIsCmt-p)
              (my-uncomment-region p1 p2)
            (my-comment-region p1 p2)
            ))
      (progn
        (if (wholeLineIsCmt-p)
            (my-uncomment-current-line)
          (my-comment-current-line)
          )) )))

(defun wholeLineIsCmt-p ()
  (save-excursion
    (beginning-of-line 1)
    (looking-at "[ \t]*#")
    ))

(defun my-comment-current-line ()
  (interactive)
  (beginning-of-line 1)
  (insert "#")
  )

(defun my-uncomment-current-line ()
  "Remove “#” (if any) in the beginning of current line."
  (interactive)
  (when (wholeLineIsCmt-p)
    (beginning-of-line 1)
    (search-forward "#")
    (delete-backward-char 2)
    ))

(defun my-comment-region (p1 p2)
  "Add “#” to the beginning of each line of selected text."
  (interactive "r")
  (let ((deactivate-mark nil))
    (save-excursion
      (goto-char p2)
      (while (>= (point) p1)
        (my-comment-current-line)
        (previous-line)
        ))))

(defun my-uncomment-region (p1 p2)
  "Remove “#” (if any) in the beginning of each line of selected text."
  (interactive "r")
  (let ((deactivate-mark nil))
    (save-excursion
      (goto-char p2)
      (while (>= (point) p1)
        (my-uncomment-current-line)
        (previous-line) )) ))

(provide 'stottr-mode)

