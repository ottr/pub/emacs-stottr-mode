# emacs-stottr-mode

Emacs mode for stottr syntax highlighting and possibly other helpful features.


## Setup

In order to use the built in expansion commands, you must set the path to your local `lutra.jar` file in the
`stottr-lutra-path` variable in your `.emacs` file:

> (setq stottr-lutra-path "path/to/lutra.jar")

Add the following in order to use `stottr-mode` for all `.stottr` files:

> (add-to-list 'auto-mode-alist '("\\.stottr\\'" . stottr-mode))

## Commands

Currently, stottr-mode has two commands for running Lutra:
`stottr-lutra-library` and `stottr-lutra-instance`.
### Expand template library
`C-c C-l` in order to run the lutra CLI library commands. Currently supported features:

- expandLibrary
- formatLibrary

### Expand template instances

`C-c C-i` in order to run the lutra cli for expanding template
instances. Currently supported features

- expand
- format

NOTE: Lutra will try to fetch all missing template definitions using
their IRI. Currently, all fetched templates must be in the wOTTR
format (WIP).

### Local variables
When running `stottr-lutra-library` or `stottr-lutra-instance`,
arguments can be saved as file local variables. The following file
local variables can be saved:

- `stottr-lutra-library`
 library-mode
 library-output-format
 library-output-location

- `stottr-lutra-instance`
 instance-mode
 instance-output-format
 instance-library-format
 instance-library-location
 instance-output-location

